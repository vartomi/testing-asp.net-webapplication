using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebApplication2.Models;
using WebApplication2.Services;

namespace WebApplication2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddDbContext<WebShopContext>(options =>
            //options.UseSqlServer(Configuration.GetConnectionString("SqlServerConnection")));
            DbType dbType = Configuration.GetValue<DbType>("DbType");

            // Database context dependency injection
            switch (dbType)
            {
                case DbType.SqlServer:
                    services.AddDbContext<WebShopContext>(options =>
                        options.UseSqlServer(Configuration.GetConnectionString("SqlServerConnection")));
                    break;
                case DbType.Sqlite:
                    services.AddDbContext<WebShopContext>(options =>
                        options.UseSqlite(Configuration.GetConnectionString("SqliteConnection")));
                    break;
            }

            services.AddTransient<IWebShopService,  WebShopService>();
            services.AddTransient<IAccountServices, AccountServices>();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(10); // session is live for max 10 mins
                options.Cookie.HttpOnly = true; // client side scripts would not achieve cookies 
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();


            //services.AddControllersWithViews();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();
            app.UseSession();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                    //pattern: "{controller=Home}/{action=Categories}/{id?}");
            });

            DbInitilalizer.Initialize(serviceProvider, Configuration.GetValue<string>("ImageStore"));
        }
    }
}
