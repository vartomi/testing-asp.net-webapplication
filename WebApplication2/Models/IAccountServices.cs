﻿using WebApplication2.Models;

namespace WebApplication2.Services
{
    public interface IAccountServices
    {
        Guest GetGuest(string userName);
    }
}
