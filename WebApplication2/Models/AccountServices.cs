﻿using System.Linq;
using WebApplication2.Models;

namespace WebApplication2.Services
{
    public class AccountServices : IAccountServices
    {
        private WebShopContext context;

        public AccountServices(WebShopContext Context)
        {
            context = Context;
        }

        public Guest GetGuest(string userName)
        {
            if (userName == null) return null;
            return context.Guests.FirstOrDefault(g => g.UserName == userName);
        }
    }
}
