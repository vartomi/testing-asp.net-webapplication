﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Newtonsoft.Json.Linq;
using Xunit.Sdk;

namespace ASPSelenium
{
    public class JSONFileDataAttribute : DataAttribute
    {
        private readonly string _file;

        public JSONFileDataAttribute(string file)
        {
            _file = file;
        }

        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            if (testMethod == null)
            {
                throw new ArgumentNullException(nameof(testMethod));
            }

            return ReadJSON.Read(_file);
        }
    }
}
