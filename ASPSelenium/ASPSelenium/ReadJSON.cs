﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;

namespace ASPSelenium
{
    public class ReadJSON
    {
        //Read JSON file and returnes it as an IEnumerable<object[]>
        //JSON file must be located next to source files
        public static IEnumerable<object[]> Read(string file)
        {
            String curdir = Directory.GetCurrentDirectory();
            var path = curdir + "\\..\\..\\..\\test_suites\\" + file;

            if (!File.Exists(path))
            {
                throw new ArgumentException($"Could not find file at path: test_suites\\{file}");
            }

            var fileData = File.ReadAllText(path);

            dynamic jsondata = JObject.Parse(fileData);
            TestObject testObject = ((JObject)jsondata.test_suite).ToObject<TestObject>();
            List<object[]> result = new List<object[]>
            {
                new object[] { 0, "home page", testObject.input_list[0], testObject.output_list[0] }
            };

            //extends the result with a "from" column which helps to initialize every testcase
            for (int i = 1; i < testObject.input_list.Count; i++)
            {
                //instead of "OK" value the last test cases output is used for "from" column where the output is not "OK" 
                object[] new_object;
                if (testObject.output_list[i - 1] == "OK")
                {
                    int j = i - 2;
                    while (j >= 0 && testObject.output_list[j] == "OK")
                    {
                        j--;
                    }
                    new_object = new object[] { i, testObject.output_list[j], testObject.input_list[i], testObject.output_list[i] };
                }
                else
                {
                    new_object = new object[] { i, testObject.output_list[i - 1], testObject.input_list[i], testObject.output_list[i] };
                }
                result.Add(new_object);
            }

            return result;
        }
    }
}
