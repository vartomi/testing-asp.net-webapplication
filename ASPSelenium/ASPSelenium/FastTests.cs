using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Reflection;
using Xunit;

namespace ASPSelenium
{
    public class FastTests
    {
        ChromeDriver driver = new ChromeDriver();

        [Fact]
        public void TestAS()
        {
            Functions.FastTest(driver, "AS_suite.json");
        }

        [Fact]
        public void TestATS0()
        {
            Functions.FastTest(driver, "ATS0_suite.json");
        }

        [Fact]
        public void TestATS4()
        {
            Functions.FastTest(driver, "ATS4_suite.json");
        }

        [Fact]
        public void TestATSa()
        {
            Functions.FastTest(driver, "ATSa_suite.json");
        }

        [Fact]
        public void TestRandom_state_80()
        {
            Functions.FastTest(driver, "Random_state-80_suite.json");
        }

        [Fact]
        public void TestRandom_transition_100()
        {
            Functions.FastTest(driver, "Random_transition-100_suite.json");
        }

        [Fact]
        public void TestTT()
        {
            Functions.FastTest(driver, "TT_suite.json");
        }

        [Fact]
        public void TestWeightedRandom_transition_100()
        {
            Functions.FastTest(driver, "WeightedRandom_transition-100_suite.json");
        }
    }
}
