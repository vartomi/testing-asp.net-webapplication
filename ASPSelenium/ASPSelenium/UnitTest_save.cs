using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using Xunit;

namespace ASPSelenium
{
    //this was the initial unit test file, now Test.cs and FastTest.cs contains the testcases
    public class UnitTest_save
    {
        readonly Dictionary<String, String> xpaths = new Dictionary<string, string>()
        {
            {"press Categories menuitem" , "/html/body/nav/div/div/a[2]"},
            {"select a category", "/html/body/div/main/table/tbody/tr[2]/td[1]/a"},
            {"press Details", "/html/body/div/main/table/tbody/tr[2]/td[6]/a"},
            {"press Add to cart", "/html/body/div/main/table/tbody/tr/td[8]/a"},
            {"press Cart menuitem", "/html/body/nav/div/div/a[3]"},
            {"press Confirm order", "/html/body/div/main/div[2]/a"},
            {"press Confirm", "/html/body/div/main/div/form/table/tbody/tr[7]/td/input"},
            {"press Previous", "/html/body/div/main/a[1]"},
            {"press Next", "/html/body/div/main/a[2]"},
            {"press Home menuitem", "/html/body/nav/div/div/a[1]"},
            {"press Producer column header", "/html/body/div/main/table/thead/tr/th[2]/a"},
            {"press Net price/List price column header", "/html/body/div/main/table/thead/tr/th[5]/a"},
            {"press Remove from cart", "/html/body/div/main/ul/li/span[3]/a"},
            {"press Delete order button", "/html/body/div/main/div[1]/a"}
        };

        readonly Dictionary<String, String> pageTitles = new Dictionary<string, string>()
        {
            {"home page" , "Home"},
            {"categories page" , "Categories"},
            {"products page" , "Products"},
            {"product page" , "Product"},
            {"cart page" , "Cart"},
            {"form page" , "Form"},
            {"confirm page" , "Successful order"}
        };

        readonly Dictionary<String, String> urls = new Dictionary<string, string>()
        {
            {"categories page" , "http://localhost:5000/Home/Categories"},
            {"products page", "http://localhost:5000/Home/Products?catId=2"},
            {"product page", "http://localhost:5000/Home/Details?prodId=45&catId=2"},
            {"OK", "http://localhost:5000/Home/AddToCart?prodId=45&catId=2"},
            {"cart page", "http://localhost:5000/Home/CartPage"},
            {"form page", "http://localhost:5000/Home/FormPage?total=80000"},
            {"confirm page", "http://localhost:5000/Home/FormPage"},
            {"home page" , "http://localhost:5000/"},
            {"sort by producer", "http://localhost:5000/Home/Products?catId=2&sortOrder=producer_asc"},
            {"sort by price", "http://localhost:5000/Home/Products?catId=2&sortOrder=netprice_desc"},
            {"cart", "http://localhost:5000/Home/CartPage"},
        };

        //[Theory]
        //[JSONFileData("TT_suite.json")]
        //[JSONFileData("test.json")]
        public void Test(int i, string from, string input, string output)
        {
            ChromeDriver driver = new ChromeDriver();

            if (input == "press Confirm order" || input == "press Confirm" || from == "confirm page"
                || input == "press Remove from cart" || input == "press Delete order button")
            {
                //For order creation testing a product needs to be added to the cart
                driver.Navigate().GoToUrl(urls["categories page"]);

                IWebElement selectCat = driver.FindElement(By.XPath(xpaths["select a category"]));
                selectCat.Click();
                WebDriverWait waitCat = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
                waitCat.Until(wt => wt.FindElement(By.XPath("/html/body/div/main/h1")));

                IWebElement pressDetails = driver.FindElement(By.XPath(xpaths["press Details"]));
                pressDetails.Click();
                WebDriverWait waitProd = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
                waitProd.Until(wt => wt.FindElement(By.XPath("/html/body/div/main/h1")));
                
                IWebElement addToCart = driver.FindElement(By.XPath(xpaths["press Add to cart"]));
                addToCart.Click();
                WebDriverWait waitAdd = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
                waitAdd.Until(wt => wt.FindElement(By.XPath("/html/body/div/main/h1")));

                if (input == "press Confirm" || from == "confirm page")
                {
                    //For order confimation we need to go to cart page, click on Confirm Order then fill in the form and click on Confirm
                    driver.Navigate().GoToUrl(urls["cart page"]);

                    IWebElement pressConfirmOrder = driver.FindElement(By.XPath(xpaths["press Confirm order"]));
                    pressConfirmOrder.Click();

                    Actions actions = new Actions(driver);
                    actions.Click(driver.FindElement(By.Id("Name")))
                        .SendKeys("test name" + Keys.Tab)
                        .SendKeys("test address" + Keys.Tab)
                        .SendKeys("0123456789" + Keys.Tab)
                        .SendKeys("test@test.com" + Keys.Tab)
                        .Build().Perform();

                    if (from == "confirm page")
                    {
                        IWebElement pressConfirm = driver.FindElement(By.XPath("/html/body/div/main/div/form/table/tbody/tr[7]/td/input"));
                        pressConfirm.Click();
                    }
                }
            }

            if (input != "press Confirm" && from != "confirm page")
            {
                try
                {
                    driver.Navigate().GoToUrl(urls[from]);
                }
                catch (Exception e)
                {
                    driver.Close();
                    throw new Exception("ERROR: Could not navigate to \"from\" URL!");
                }
            }

            try
            {
                if (input == "press Previous")
                {
                    IWebElement pressNext = driver.FindElement(By.XPath("/html/body/div/main/a[2]"));
                    pressNext.Click();
                }

                IWebElement nextAction = driver.FindElement(By.XPath(xpaths[input]));
                nextAction.Click();
            }
            catch (Exception e)
            {
                driver.Close();
                throw new Exception("ERROR: Could not find element by XPath on source page!");
            }

            try
            {
                WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
                wait.Until(wt => wt.FindElement(By.XPath("/html/body/div/main/h1")));
            }
            catch (Exception e)
            {
                driver.Close();
                throw new Exception("ERROR: Could not find element by XPath on target page!");
            }

             IWebElement toPageTitle;
             try
             {
                 toPageTitle = driver.FindElement(By.XPath("/html/body/div/main/h1"));
             }
             catch (Exception e)
             {
                 driver.Close();
                 throw new Exception("ERROR: Could not find page title on target page!");
             }

            if (String.Equals(output,"OK"))
            {
                Assert.Equal(pageTitles[from], toPageTitle.Text);
            }
            else
            {
                Assert.Equal(pageTitles[output], toPageTitle.Text);
            }

            driver.Close();
        }
    }
}
