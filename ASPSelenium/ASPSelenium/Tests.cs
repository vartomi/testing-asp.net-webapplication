using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Reflection;
using Xunit;

namespace ASPSelenium
{
    //To run these tests [Theory] annotations have to be uncommented
    public class Tests
    {
        //[Theory]
        [JSONFileData("AS_suite.json")]
        public void TestAS(int i, string from, string input, string output)
        {
            Functions.Test(i, from, input, output);
        }

        //[Theory]
        [JSONFileData("ATS0_suite.json")]
        public void TestATS0(int i, string from, string input, string output)
        {
            Functions.Test(i, from, input, output);
        }

        //[Theory]
        [JSONFileData("ATS4_suite.json")]
        public void TestATS4(int i, string from, string input, string output)
        {
            Functions.Test(i, from, input, output);
        }

        //[Theory]
        [JSONFileData("ATSa_suite.json")]
        public void TestATSa(int i, string from, string input, string output)
        {
            Functions.Test(i, from, input, output);
        }

        //[Theory]
        [JSONFileData("Random_state-80_suite.json")]
        public void TestRandom_state_80(int i, string from, string input, string output)
        {
            Functions.Test(i, from, input, output);
        }

        //[Theory]
        [JSONFileData("Random_transition-100_suite.json")]
        public void TestRandom_transition_100(int i, string from, string input, string output)
        {
            Functions.Test(i, from, input, output);
        }

        //[Theory]
        [JSONFileData("TT_suite.json")]
        public void TestTransitionTour(int i, string from, string input, string output)
        {
            Functions.Test(i, from, input, output);
        }

        //[Theory]
        [JSONFileData("WeightedRandom_transition-100_suite.json")]
        public void TestWeightedRandom_transition_100(int i, string from, string input, string output)
        {
            Functions.Test(i, from, input, output);
        }
    }
}
