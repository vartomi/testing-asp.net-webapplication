﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using Xunit;

namespace ASPSelenium
{
    public class Dictionaries
    {
        //maps the inputs to the HTML element XPaths
        public static Dictionary<String, String> xpaths = new Dictionary<string, string>()
        {
            {"press Categories menuitem" , "/html/body/nav/div/div/a[2]"},
            {"select a category", "/html/body/div/main/table/tbody/tr[2]/td[1]/a"},
            {"press Details", "/html/body/div/main/table/tbody/tr[2]/td[6]/a"},
            {"press Add to cart", "/html/body/div/main/table/tbody/tr/td[8]/a"},
            {"press Cart menuitem", "/html/body/nav/div/div/a[3]"},
            {"press Confirm order", "/html/body/div/main/div[2]/a"},
            {"press Confirm", "/html/body/div/main/div/form/table/tbody/tr[7]/td/input"},
            {"press Previous", "/html/body/div/main/a[1]"},
            {"press Next", "/html/body/div/main/a[2]"},
            {"press Home menuitem", "/html/body/nav/div/div/a[1]"},
            {"press Producer column header", "/html/body/div/main/table/thead/tr/th[2]/a"},
            {"press Net price/List price column header", "/html/body/div/main/table/thead/tr/th[5]/a"},
            {"press Remove from cart", "/html/body/div/main/ul/li/span[3]/a"},
            {"press Delete order button", "/html/body/div/main/div[1]/a"}
        };

        //maps the output values to the page titles
        public static Dictionary<String, String> pageTitles = new Dictionary<string, string>()
        {
            {"home page" , "Home"},
            {"categories page" , "Categories"},
            {"products page" , "Products"},
            {"product page" , "Product"},
            {"cart page" , "Cart"},
            {"form page" , "Form"},
            {"confirm page" , "Successful order"}
        };

        //maps the output values to the URLs
        public static Dictionary<String, String> urls = new Dictionary<string, string>()
        {
            {"categories page" , "http://localhost:5000/Home/Categories"},
            {"products page", "http://localhost:5000/Home/Products?catId=2"},
            {"product page", "http://localhost:5000/Home/Details?prodId=45&catId=2"},
            {"OK", "http://localhost:5000/Home/AddToCart?prodId=45&catId=2"},
            {"cart page", "http://localhost:5000/Home/CartPage"},
            {"form page", "http://localhost:5000/Home/FormPage?total=80000"},
            {"confirm page", "http://localhost:5000/Home/FormPage"},
            {"home page" , "http://localhost:5000/"},
            {"sort by producer", "http://localhost:5000/Home/Products?catId=2&sortOrder=producer_asc"},
            {"sort by price", "http://localhost:5000/Home/Products?catId=2&sortOrder=netprice_desc"},
            {"cart", "http://localhost:5000/Home/CartPage"},
        };
    }
}
