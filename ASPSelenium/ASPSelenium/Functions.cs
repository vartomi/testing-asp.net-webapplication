﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using Xunit;

namespace ASPSelenium
{
    public class Functions
    {
        //run a testsequence in one browser
        public static void FastTest(ChromeDriver driver, string json_name)
        {
            IEnumerable<object[]> data = ReadJSON.Read(json_name);

            foreach (object[] d in data)
            {
                int i = (int)d[0];
                string from = d[1].ToString();
                string input = d[2].ToString();
                string output = d[3].ToString();
                //these inputs require adding a product to the cart
                if (input == "press Confirm order" || input == "press Confirm" || from == "confirm page"
                    || input == "press Remove from cart" || input == "press Delete order button")
                {
                    Functions.AddToCart(driver);

                    //these inputs require filling the form
                    if (input == "press Confirm" || from == "confirm page")
                    {
                        Functions.FillForm(driver, from);
                    }
                }

                Functions.NavigateToURL(driver, i, from, input, output);

                Functions.MakeClick(driver, i, from, input, output);

                Functions.WaitClick(driver, i, from, input, output);

                Functions.CheckOK(driver, i, from, input, output);
            }
            driver.Close();
        }

        //run a test with one testcase
        public static void Test(int i, string from, string input, string output)
        {
            ChromeDriver driver = new ChromeDriver();

            //these inputs require adding a product to the cart
            if (input == "press Confirm order" || input == "press Confirm" || from == "confirm page"
                || input == "press Remove from cart" || input == "press Delete order button")
            {
                Functions.AddToCart(driver);

                //these inputs require filling the form
                if (input == "press Confirm" || from == "confirm page")
                {
                    Functions.FillForm(driver, from);
                }
            }

            Functions.NavigateToURL(driver, i, from, input, output);

            Functions.MakeClick(driver, i, from, input, output);

            Functions.WaitClick(driver, i, from, input, output);

            Functions.CheckOK(driver, i, from, input, output);

            driver.Close();
        }

        //simulate the addition of a product to the cart 
        public static void AddToCart(ChromeDriver driver)
        {
            GoToCategoriesPage(driver);

            IWebElement selectCat = driver.FindElement(By.XPath(Dictionaries.xpaths["select a category"]));
            selectCat.Click();
            WebDriverWait waitCat = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
            waitCat.Until(wt => wt.FindElement(By.XPath("/html/body/div/main/h1")));

            IWebElement pressDetails = driver.FindElement(By.XPath(Dictionaries.xpaths["press Details"]));
            pressDetails.Click();
            WebDriverWait waitProd = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
            waitProd.Until(wt => wt.FindElement(By.XPath("/html/body/div/main/h1")));

            IWebElement addToCart = driver.FindElement(By.XPath(Dictionaries.xpaths["press Add to cart"]));
            addToCart.Click();
            WebDriverWait waitAdd = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
            waitAdd.Until(wt => wt.FindElement(By.XPath("/html/body/div/main/h1")));
        }

        //simulate filling of the from
        public static void FillForm(ChromeDriver driver, string from)
        {
            //For order confimation we need to go to cart page, click on Confirm Order then fill in the form and click on Confirm
            driver.Navigate().GoToUrl(Dictionaries.urls["cart page"]);

            IWebElement pressConfirmOrder = driver.FindElement(By.XPath(Dictionaries.xpaths["press Confirm order"]));
            pressConfirmOrder.Click();

            Actions actions = new Actions(driver);
            actions.Click(driver.FindElement(By.Id("Name")))
                .SendKeys("test name" + Keys.Tab)
                .SendKeys("test address" + Keys.Tab)
                .SendKeys("0123456789" + Keys.Tab)
                .SendKeys("test@test.com" + Keys.Tab)
                .Build().Perform();

            if (from == "confirm page")
            {
                IWebElement pressConfirm = driver.FindElement(By.XPath("/html/body/div/main/div/form/table/tbody/tr[7]/td/input"));
                pressConfirm.Click();
            }
        }

        //go to categories page
        public static void GoToCategoriesPage(ChromeDriver driver)
        {
            driver.Navigate().GoToUrl(Dictionaries.urls["categories page"]);
        }

        //navigate to given URL
        public static void NavigateToURL(ChromeDriver driver, int i, string from, string input, string output)
        {
            if (input != "press Confirm" && from != "confirm page")
            {
                try
                {
                    driver.Navigate().GoToUrl(Dictionaries.urls[from]);
                }
                catch (Exception e)
                {
                    driver.Close();
                    throw new Exception("ERROR: Could not navigate to \"from\" URL!" +
                        "\n" + "test No.: " + i.ToString() + ", test from: " + from + ", test input: " + input + ", test output: " + output);
                }
            }
        }

        //simulate clicking on an element
        public static void MakeClick(ChromeDriver driver, int i, string from, string input, string output)
        {
            try
            {
                if (input == "press Previous")
                {
                    IWebElement pressNext = driver.FindElement(By.XPath("/html/body/div/main/a[2]"));
                    pressNext.Click();
                }
                else if(input == "select a category")
                {
                    Functions.GoToCategoriesPage(driver);
                }

                IWebElement nextAction = driver.FindElement(By.XPath(Dictionaries.xpaths[input]));
                nextAction.Click();
            }
            catch (Exception e)
            {
                //driver.Close();
                throw new Exception("ERROR: Could not find element by XPath on source page!" + 
                    "\n" + "test No.: " + i.ToString() + ", test from: " + from + ", test input: " + input + ", test output: " + output);
            }
        }

        //wait for the webpage to reload after click
        public static void WaitClick(ChromeDriver driver, int i, string from, string input, string output)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
                wait.Until(wt => wt.FindElement(By.XPath("/html/body/div/main/h1")));
            }
            catch (Exception e)
            {
                driver.Close();
                throw new Exception("ERROR: Could not find element by XPath on target page!" +
                    "\n" + "test No.: " + i.ToString() + ", test from: " + from + ", test input: " + input + ", test output: " + output);
            }
        }

        //check assertion
        public static void CheckOK(ChromeDriver driver, int i, string from, string input, string output)
        {
            IWebElement toPageTitle;
            try
            {
                toPageTitle = driver.FindElement(By.XPath("/html/body/div/main/h1"));
            }
            catch (Exception e)
            {
                driver.Close();
                throw new Exception("ERROR: Could not find page title on target page!" +
                    "\n" + "test No.: " + i.ToString() + ", test from: " + from + ", test input: " + input + ", test output: " + output);
            }

            if (String.Equals(output, "OK"))
            {
                Assert.Equal(Dictionaries.pageTitles[from], toPageTitle.Text);
            }
            else
            {
                Assert.Equal(Dictionaries.pageTitles[output], toPageTitle.Text);
            }
        }
    }
}
