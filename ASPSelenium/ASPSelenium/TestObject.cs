﻿using System;
using System.Collections.Generic;

namespace ASPSelenium
{
    public class TestObject
    {
        public String name { get; set; }
        public String id { get; set; }
        public String method { get; set; }
        public IList<String> transition_list { get; set; }
        public IList<String> input_list { get; set; }
        public IList<String> output_list { get; set; }
    }
}
