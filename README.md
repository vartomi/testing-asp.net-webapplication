# About the project
This project is about model-based testing an ASP.NET web application. The project consists of two separate subprojects: the ASP.NET subproject and the testing subproject.

## The ASP.NET subproject:
The ASP.NET web application is the basis for the project, it is the SUT (System Under Test). I have created it as an assignment for my university studies. The application is a simplified version of a webshop selling electrical devices. The application allows the users to make orders for the items that are available in the store. This is located in the **WebApplication2** folder.

## The testing subproject:
The testing subproject is the adaptation code for the SUT. I used MBT (Model-based testing) to test specifically the navigation between the menu pages and the order of the webshop items in the web application. This is located in the **ASPSelenium** folder.

### The model:
I have created the model, a FSM (Finite State Machine), based on the specification of the assignment using [GraphWalker](https://graphwalker.github.io/). This is located in the **WebShop.json** file.

### The test suites:
Using the model I have created by GraphWalker, I have chosen the [Model>Test>Relax](https://gitlab.inf.elte.hu/nga/ModelTestRelax.git) MBT framework to generate the test suites with different test generation algorithms. These are the json files located in the **test_suites** folder.

### The adaptation code:
I have used Selenium for the purpose of testing the web application. Selenium is able to click, navigate and fill in the web application HTML elements using their xPath so it can be used as a robot for testing.
I have created a separate xUnit Test Project named **ASPSelenium** in Visual Studio. When testing is happening, I iterate over the test suite files input list and output list and read them into objects so the adaptation code can adapt the data. After this, I simulate the proper actions related to the input list elements and check if the desired outcome related to the output list elements matches the actual outcome. If they match, the test suite is successful.

# Guide for testing:
First the web application subproject has to run. For this the **WebApplication2.sln** solution file has to be executed. This will open a Microsoft Visual Studio instance. After this the application will start by pressing the **IIS Express** button with the green triangle.

The next step is to get into the ASPSelenium folder. Here the **ASPSelenium.sln** solution file has to be executed. This will open another instance of a Visual Studio. Here the **Test** > **Run All Tests** menu item has to be selected. This will open up the **Test Explorer**. Here the status of the tests can be monitored. 

The tests are located in the Tests.cs and FastTests.cs files. Both files run the same tests but require different runtime and provide differently detailed test cases.

In Tests.cs file we can see 8 xUnit tests annoted as **Theories**. Since these tests are actually test suites, when running these tests in the Test Explorer, all the test cases with their parameters will be listed under one test suite. Running these tests require about 6 times more runtime, since all the tests from every test suite is tested and evaluated separately. A new browser openes up for every test and closes at its evaluation. 

In FastTests.cs file we can also see 8 xUnit tests annotated as **Facts**. These tests are also test suites however when running these tests in the Test Explorer only the test suites are listed. Running these tests takes less time because only one browser is opened for every test suite.

By default I commented the Theory annotations in Tests.cs so Test Explorer will only execute the tests from FastTests.cs. To execute tests from Tests.cs the comments have to be removed from the Theory annotations.